django-filter==2.3.0
djangorestframework==3.11.1
Markdown==3.2.2
django-cors-headers==3.4.0
django-dotenv==1.4.2
psycopg2==2.8.5
