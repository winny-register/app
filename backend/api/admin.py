from django.contrib import admin

from django.contrib.auth.admin import UserAdmin
from .models import User

from rest_framework.authtoken.admin import TokenAdmin

from .models import Product, Transaction


admin.site.register(User, UserAdmin)
TokenAdmin.raw_id_fields = ['user']
admin.site.register(Product)
admin.site.register(Transaction)
