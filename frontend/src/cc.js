function cc(change, denominations = [20000, 10000, 500, 100, 25, 10, 5, 1]) {
  return denominations.slice().sort().reduce(({change, ...acc}, denomination) => {
    acc[denomination] = Math.floor(change / denomination)
    acc.change = change % denomination
  }, {change:change})
}

exports.cc = cc;
